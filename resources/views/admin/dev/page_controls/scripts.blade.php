<script id="formControlTemplate" type="text/x-handlebars-template">
	<div class="flex margin-bottom form-control-wrapper" sortable-id="@{{ id }}">
		<div class="flex-auto form-input-icon-control pad-right no-left-padding">
			<a href="#">
				<i class="far fa-ellipsis-v hover-scale" role="button" data-toggle="tooltip" title="Drag and Reorder"></i>
			</a>
		</div>
		<div class="flex-1">
			<div class="caboodle-form-group">
				<input type="hidden" name="control[]" value="@{{ id }}">
				<label for="name[]" class="less-margin x-small">Name</label>
				{!! Form::text('name[]', null, ['class'=>'form-control', 'placeholder'=>'', 'autocomplete'=>'off', 'required']) !!}
			</div>
		</div>
		<div class="flex-1">
			<div class="caboodle-form-group">
				<label for="label[]" class="less-margin x-small">Label</label>
				{!! Form::text('label[]', null, ['class'=>'form-control', 'placeholder'=>'', 'autocomplete'=>'off', 'required']) !!}
			</div>
		</div>
		<div class="flex-1">
			<div class="caboodle-form-group margin-bottom">
				<label for="type[]" class="less-margin x-small">Form Control Type</label>
				<select name="type[]" class="form-control" autocomplete="off" required>
					<option value="text">Text</option>
					<option value="number">Number</option>
					<option value="checkbox">Checkbox</option>
					<option value="textarea">Textarea</option>
					<option value="asset">Asset</option>
					<option value="select">Select</option>
					<option value="date">Date</option>
					<option value="time">Time</option>
					<option value="date_time">Date Time</option>
					<!-- <option value="products">Products Selector</option> -->
				</select>
			</div>
			<div class="caboodle-form-group caboodle-form-group-code-editor hide">
				<label for="options_json[]" class="less-margin xx-small">Options in JSON</label>
				<textarea readonly name="options_json[]" class="form-control" role="button"></textarea>
			</div>
		</div>
		<div class="flex-auto margin-5">
			<div class="caboodle-form-group">
				<label for="required[]" class="less-margin x-small" value="@{{ id }}">Required</label>
				<div class="mdc-form-field caboodle-flex caboodle-flex-center" width="40px">
					<div class="mdc-checkbox no-margin">
						<input type="checkbox" name="required[]" class="mdc-checkbox__native-control" value="@{{ id }}" />
						<div class="mdc-checkbox__background">
							<svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
								<path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
							</svg>
							<div class="mdc-checkbox__mixedmark"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="flex-auto form-input-icon-control">
			<a href="#" class="edit-control" data-id="@{{ id }}">
				<i class="far fa-edit hover-scale" role="button" data-toggle="tooltip" title="Edit"></i>
			</a>
		</div>
		<div class="flex-auto form-input-icon-control">
			<a href="#" class="delete-control" data-id="@{{ id }}">
				<i class="far fa-trash hover-scale color-red" role="button" data-toggle="tooltip" title="Delete"></i>
			</a>
		</div>
	</div>
</script>

<div class="modal fade" id="selectOptionsSettings">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
			<form>
				<input type="hidden" name="control_id">
				<div class="modal-header">
					<h4 class="modal-title">Select Options</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<input type="hidden" name="tmp_options_json">
					<a href=""></a>
					<a href="#" class="btn-transparent btn-sm flex-1 text-right" data-toggle="add-option">
						<i class="far fa-plus-circle"></i> Add Option
					</a>
					<br><br>
					<div class="form-options-container form-controls-sortable"></div>
				</div>

				<div class="modal-footer">
					<button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button mdc-ripple-upgraded" data-dismiss="modal">Cancel</button>
					<button type="submit" class="apply-btn caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Apply</button>
				</div>
			</form>
        </div>
    </div>
</div>

<div class="modal fade" id="editValueModal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
			<form>
				<input type="hidden" name="control_id">
				<div class="modal-header">
					<h4 class="modal-title">Change Value</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<br><br>
					<div class="form-options-container">
						<div id="sample"></div>
					</div>
					<?php
					// <div class="caboodle-form-group sumo-asset-select" data-crop-url="{{route('adminCaboodleProductsCropUrl')}}">
					// 	<label for="asset[]" class="caboodle-flex caboodle-flex-space-between caboodle-flex-align-center">Asset</label>
					// 	{!! Form::hidden('asset[]', null, ['class'=>'sumo-asset', 'data-id'=>'', 'data-thumbnail'=>'image_thumbnail']) !!}
					// </div>
					?>
				</div>

				<div class="modal-footer">
					<button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button mdc-ripple-upgraded" data-dismiss="modal">Cancel</button>
					<button type="submit" class="apply-btn caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Apply</button>
				</div>
			</form>
        </div>
    </div>
</div>

<script id="selectOptionsTemplate" type="text/x-handlebars-template">
	<div class="flex margin-bottom form-control-wrapper" sortable-id="@{{ id }}">
		<div class="flex-auto form-input-icon-control pad-right no-left-padding" style="padding-top: 38px">
			<a href="#">
				<i class="far fa-ellipsis-v hover-scale" role="button" data-toggle="tooltip" title="Drag and Reorder"></i>
			</a>
		</div>
		<div class="flex-1">
			<div class="caboodle-form-group">
				<label for="option_value[]" class="less-margin x-small">Value</label>
				<input type="text" name="option_value[]" class="form-control" autocomplete="off" required value="@{{ value }}"/>
			</div>
		</div>
		<div class="flex-1">
			<div class="caboodle-form-group">
				<label for="option_label[]" class="less-margin x-small">Label</label>
				<input type="text" name="option_label[]" class="form-control" autocomplete="off" required value="@{{ label }}"/>
			</div>
		</div>
		<div class="flex-auto form-input-icon-control" style="padding-top: 38px">
			<a href="#" class="delete-option" data-id="@{{ id }}">
				<i class="far fa-trash hover-scale color-red" role="button" data-toggle="tooltip" title="Delete"></i>
			</a>
		</div>
	</div>
</script>

<!-- TEXT AND NUMBER -->

<script id="editValue" type="text/x-handlebars-template">
	<div class="flex margin-bottom form-control-wrapper" sortable-id="@{{ id }}">
		<div class="flex-1">
			<div class="caboodle-form-group">
				<label for="value" class="less-margin x-small">Value</label>
				<input type="@{{type}}" name="value" class="form-control" autocomplete="off" required value="@{{ value }}"/>
			</div>
		</div>
	</div>
</script>

<!-- CHECKBOX -->

<script id="editValueCheckbox" type="text/x-handlebars-template">
	<div class="caboodle-form-group">
		<div class="mdc-form-field caboodle-flex caboodle-flex-start" width="40px">
		<label for="required[]" class="less-margin x-small" value="@{{ id }}">@{{label}}</label>
			<div class="mdc-checkbox no-margin">	
				<input type="checkbox" name="required[]" class="mdc-checkbox__native-control" value="@{{ id }}" />
				<div class="mdc-checkbox__background">
					<svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
						<path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
					</svg>
					<div class="mdc-checkbox__mixedmark"></div>
				</div>
			</div>
		</div>
	</div>
</script>

<!-- DATE / TIME / DATE TIME -->

<script id="editDateTime" type="text/x-handlebars-template">
	<div class="flex margin-bottom form-control-wrapper" sortable-id="@{{ id }}">
		<div class="flex-1">
			<div class="caboodle-form-group">
				<label for="value" class="less-margin x-small">Value</label>
				<input type="@{{type}}" name="value" class="form-control @{{class}}" autocomplete="off" required value="@{{ value }}"/>
			</div>
		</div>
	</div>
</script>



<script>
	var emptyInfo = $('.empty-form-controls');
	var container = $('.form-controls-container');
	var formControlTemplate = Handlebars.compile($("#formControlTemplate").html());
	$('.add-form-control-toggle').on('click', function (e) {
		e.preventDefault();
		emptyInfo.addClass('hide');
		var tmpId = guid();
		container.append(formControlTemplate({ id: tmpId }));
		$('[sortable-id="' + tmpId + '"]').find('[name="label[]"]').focus();
	});

	$('body').on('click', '.delete-control', function(e) {
		e.preventDefault();
		var id = $(this).data('id');
		swal({
			title: "Delete form control?",
			text: 'This action cannot be reversed after saving.',
			type: "warning",
			html: true,
			showCancelButton: true,
			confirmButtonColor: "#dc4f34",
			confirmButtonText: "Delete",
			cancelButtonText: "Cancel",
		}, function (isConfirm) {
			if (isConfirm) {
				$('.form-control-wrapper[sortable-id="' + id + '"]').remove();
				if ($('[name="control[]"]').length == 0) {
					emptyInfo.removeClass('hide');
				}
			}
		});
	});

	$('body').on('change', '[name="type[]"]', function(e) {
		var controlWrapper = $(this).closest('.form-control-wrapper');
		if ($(this).val() == 'select') {
			controlWrapper.find('[name="options_json[]"]').closest('.caboodle-form-group').removeClass('hide');
		} else {
			controlWrapper.find('[name="options_json[]"]').closest('.caboodle-form-group').addClass('hide');
		}
	});
	
	$(".form-controls-sortable").sortable({
		placeholder: ".form-controls-sortable .control",
		cursor: "move",
		update: function (event, ui) {
			//puts the sortable ID in an array
			var order = $(this).sortable('serialize', {
				attribute: 'sortable-id',//this will look up this attribute
				// key: 'order',//this manually sets the key
			});
			console.log(order);
		}
	});

	var selectOptionsTemplate = Handlebars.compile($("#selectOptionsTemplate").html());
	$('body').on('click', '[name="options_json[]"]', function(e) {
		e.preventDefault();
		$('#selectOptionsSettings').modal('show');
		$('#selectOptionsSettings [name="control_id"]').val($(this).closest('.form-control-wrapper').attr('sortable-id'));
		var options = [];
		try {
			options = JSON.parse($(this).val());
		} catch (error) {}
		$('#selectOptionsSettings .form-options-container').html('');
		for (var i = 0; i < options.length; i++) {
			var tmpId = guid();
			$('#selectOptionsSettings .form-options-container').append(selectOptionsTemplate({ id: tmpId, value: options[i].value, label: options[i].label }));
		}
	});

	$('#selectOptionsSettings [data-toggle="add-option"]').on('click', function(e) {
		e.preventDefault();
		var tmpId = guid();
		$('#selectOptionsSettings .form-options-container').append(selectOptionsTemplate({ id: tmpId, value: '', label: '' }));
		$('#selectOptionsSettings .form-options-container [sortable-id="' + tmpId + '"]').find('[name="option_value[]"]').focus();
	});

	$('#selectOptionsSettings .form-options-container').on('click', '.delete-option', function(e) {
		e.preventDefault();
		var id = $(this).data('id');
		$('#selectOptionsSettings .form-options-container .form-control-wrapper[sortable-id="' + id + '"]').remove();
	});

	$('#selectOptionsSettings form').on('submit', function(e) {
		e.preventDefault();
		var controlID = $('#selectOptionsSettings [name="control_id"]').val();
		var optionsWrapper = $('#selectOptionsSettings .form-options-container .form-control-wrapper');
		var options = [];
		for (let i = 0; i < optionsWrapper.length; i++) {
			options.push({
				value: $(optionsWrapper[i]).find('[name="option_value[]"]').val(),
				label: $(optionsWrapper[i]).find('[name="option_label[]"]').val()
			});
		}
		$('.form-control-wrapper[sortable-id="' + controlID + '"] [name="options_json[]"]').val(JSON.stringify(options));
		$('#selectOptionsSettings').modal('hide');
	});


	// EDIT MODAL FOR VALUE

	$('body').on('click', '.edit-control', function(e) {
		e.preventDefault();
		var id = $(this).data('id');
		
		$('#editValueModal').modal('show');
		var label = $(this).closest('[sortable-id="' + id + '"]').find('[name="label[]"]').val();
		var type = $(this).closest('[sortable-id="' + id + '"]').find('[name="type[]"]').val();
		
		var valueTemplate = Handlebars.compile($("#editValue").html());
		var valueTemplateCheckbox = Handlebars.compile($("#editValueCheckbox").html());
		var valueTemplateDateTime = Handlebars.compile($("#editValueDateTime").html());

		if(type == 'number' || type == "text"){
			$('#editValueModal .form-options-container').html(valueTemplate({ 'type':type}));
		}
		
		if(type == 'checkbox'){
			$('#editValueModal .form-options-container').html(valueTemplateCheckbox({'type':type, 'label': label }));
		}

		if(type == 'date' || type == 'time' || type == 'date_time') {
			var classNAme = (type == 'date_time' ? 'datetime' : type) + "_picker";
			$('#editValueModal .form-options-container').html(valueTemplateDateTime({'type':type, 'label': label, 'class': className }));
		}
		
		$('#sample').redactor();
	});


</script>