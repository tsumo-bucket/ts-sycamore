<div class="modal fade" id="video-banner-modal" tabindex="-1" role="dialog" aria-labelledby="assetsModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Select Video</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <!-- <div class="row loader-asset">
            <div class="col-sm-12">
              <div class="loader">
                <div class="display-table">
                  <div class="display-cell">
                    <div class="spinner"></div>
                  </div>
                </div>
              </div>
            </div>
          </div> -->
          <div class="row row-asset">
            <div class="col-sm-8">
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="nav-item">
                  <a class="nav-link active" href="#video-library" aria-controls="video-library" role="tab" data-toggle="tab" id="libraryToggle">Video Library</a>
                </li>
                <li role="presentation" class="nav-item">
                  <a class="nav-link" href="#youtube" aria-controls="youtube" role="tab" data-toggle="tab" id="youtubeToggle">Youtube</a>
                </li>
                <li role="presentation" class="nav-item">
                  <a class="nav-link" href="#vimeo" aria-controls="vimeo" role="tab" data-toggle="tab" id="vimeoToggle">Vimeo</a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- <h5 class="mt-2">Videos from Library</h5> -->
                <div role="tabpanel" class="tab-pane active" id="video-library">
                  <div class="files videoLibrary mt-3" data-url="{{route('getUploadedVideos')}}">
                  </div>
                  <hr/>
                  <h5 class="mt-2">Video Frames</h5>
                  <div class="files videoSavedLibrary mt-3" data-url="{{route('adminVideosLibrary')}}">
                  </div>
                  
                </div>
                <div role="tabpanel" class="tab-pane" id="youtube">
                  {!! Form::text('youtube', 'nNyMIBj3DYU', ['class'=>'form-control mt-3', 'id'=>'txtYoutube', 'placeholder'=>'Youtube Code']) !!}
                  <div class="files youtubeLibrary mt-3" data-url="{{route('adminVideosYoutube')}}">
                    
                  </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="vimeo">
                  {!! Form::text('vimeo', 375324444, ['class'=>'form-control mt-3', 'id'=>'txtVimeo', 'placeholder'=>'Vimeo Code']) !!}
                  <div class="files vimeoLibrary mt-3" id="vimeoLibrary" data-url="{{route('adminVideosVimeo')}}">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-4 video-options hide" id="video_options">
              <div class="loader loader-detail hide">
                <div class="display-table">
                  <div class="display-cell">
                    <div class="spinner"></div>
                  </div>
                </div>
              </div>
              
              <div class="video-details" data-url="">
                <div class="header">Video Options</div>
                  <div class="video-preview">
                    <iframe width="100%" src="" data-frame="upload" id="videoFrame" allow="" frameborder="0"></iframe>
                  </div>
                  {!! Form::open(['route'=>'adminVideoOptionsDeleteVideo', 'id'=>'video-banner-delete', 'action'=>'POST']) !!}
                    <input type="hidden" name='id' id="deleteVideo" data-frame="" value="">
                    <input type="hidden" id="deleteTypeVideo" name="type" value ="upload">
                    <span class="delete-btn"  id="sumoVideoBannerDelete">
                      <a class="caboodle-link" href="#">Delete</a>
                    </span>
                    <span class="confirm-btn hide">
                      Are you sure?
                      <a class="confirm-delete-btn" href="{{route('adminAssetsDestroy')}}">Yes</a>
                      &middot;
                      <a href="#">No</a>
                      </br><span style="margin-right:3px;" class="fa fa-exclamation-triangle"></span>If you delete this file it will affect web content connected to this.
                    </span>
                  {!! Form::close() !!}
                <div class="video-options">
                  {!! Form::open(['route'=>'adminVideoOptionsStore', 'id'=>'video-banner-form', 'action'=>'POST']) !!}
                  <button type="button" id = "uncheck" class="btn btn-primary mt-3">Uncheck all</button>
                  <input type="hidden" id="dataframe" name="frametag" value ="" >
                  <input type="hidden" id="video-type" id="dataframe" name="type" value ="upload" >
                  @include('admin.modals.video-options')
                  <button type="submit" id="saveOptions"  class="btn btn-primary">Save</button>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" id="selectedVideo" data-pic="{{asset('img/admin/file-video.png')}}" data-frame="" value ="" >
        <input type="hidden" id="uploadVideoSource" data-source="" value ="" >
        <button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-dismiss="modal">Cancel</button>
        <!-- <button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated btn-select" data-dismiss="modal">Select</button> -->
        <button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated btn-select" id="btnVideoSelect" data-dismiss="modal">Select</button>

      </div>
    </div>
  </div>
</div>

                  <!-- <button type="submit" class="btn btn-primary">Save</button> -->