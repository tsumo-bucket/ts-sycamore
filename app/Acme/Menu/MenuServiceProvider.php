<?php

namespace App\Acme\Menu;

use Illuminate\Support\ServiceProvider;

class MenuServiceProvider extends ServiceProvider 
{
	public function register()
	{
		$this->app->bind('menu', 'App\Acme\Menu\Menu');
	}
}