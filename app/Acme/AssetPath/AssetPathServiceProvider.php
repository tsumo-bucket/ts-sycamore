<?php

namespace Acme\AssetPath;

use Illuminate\Support\ServiceProvider;

class AssetPathServiceProvider extends ServiceProvider 
{
	public function register()
	{
		$this->app->bind('assetpath', '\Acme\AssetPath\AssetPath');
	}
}